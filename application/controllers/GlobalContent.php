<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GlobalContent extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $this->load->view('header');
        $this->load->view('web_content');
        $this->load->view('footer');
    }
    
    

    public function getWallData() {

        $page = ((int) $this->input->get('page')) - 1;

        $params = array(
            'filterType' => '0',
            'limit' => '20',
            'offSet' => 20 * $page,
            'groupId' => GROUP_ID,
            'userId' => '32818',
            'tag' => ''
        );

        $headers = array(
            "api-key: " . API_KEY
        );
        $url = APIURL . "awall/getWallContent";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, 6);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


        $output = curl_exec($ch);

        echo $output;
        curl_close($ch);
        return $output;
    }

    public function comment($type, $id) {

        $data = array(
            'id' => $id,
            'type' => $type
        );

        $this->load->view('header');
        $this->load->view('comments', $data);
        $this->load->view('footer');
        return;


        // $this->load->view('errors/html/error_404');
    }

    public function getComments() {

        // echo "id ".$this->input->get('id');
        $params = array(
            'filterType' => '0',
            'limit' => '100',
            'offSet' => '0',
            'groupId' => GROUP_ID,
            'userId' => '32818',
            'tag' => '',
            'discussionId' => $this->input->get('id'),
            'pollId' => $this->input->get('id'),
            'collectionName' => 'anonymous'
        );

        $headers = array(
            "api-key: " . API_KEY
        );
        if ($this->input->get('type') === "post") {
            $url = APIURL . "awall/getComments";
        } else if ($this->input->get('type') === "poll") {
            $url = APIURL . "apolls/getPollComments";
        }

        //echo $url;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, 8);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


        $output = curl_exec($ch);
        if (empty($this->input->get('deeplink'))) {
            echo $output;
        }
        curl_close($ch);
        return $output;
    }

    public function getDeepLinkUrl() {

        $url = $this->input->post('url');
        $json = $this->getComments("0");
        //echo $data ;
        $data = json_decode($json);
        $type = $data->pData[0]->type;

        $icon = 'https://s3-us-west-2.amazonaws.com/imghush/Hush512.png';

        if ($type === "discussion") {
            $title = $data->pData[0]->subject;
            $description = $data->pData[0]->post;
            if (array_key_exists("imageUrl",$data->pData[0])) {
                $icon = $data->pData[0]->imageUrl;
            }
        } else if ($type === "poll") {
            $title = $data->pData[0]->poll;
            $description = "";
        }




        $ch = curl_init('https://api.branch.io/v1/url');
        $payload = json_encode([
            'branch_key' => BRANCH_IO_KEY,
            'campaign' => 'HushInsites',
            'channel' => 'facebook',
            'data' => [
                '$desktop_url' => $url,
                '$og_title' => $title,
                '$og_description' => $description,
                '$og_image_url' => $icon]
        ]);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        # Send request.
        $result = curl_exec($ch);
        curl_close($ch);
        //return json_decode($result);
        echo $result;
    }

}
