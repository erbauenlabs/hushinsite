
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo BASEURL; ?>/img/icons/favicon-16x16.png">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="Hush is an anonymous office talk app.Users get an exclusive private office network for themselves and coworkers. Discuss Increments, LayOffs, Promotions, New Manager and Company Strategy with your coworkers. No hierarchy">
        <meta name="keywords" content="Office,Talk,Anonymous,Workplace,Layoff,Salary,Promotion,Glassdoor">
        <title>Hush &#8211; Real Voice of Employees, Get company insights from verified coworkers</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <link rel='stylesheet' id='font-awesome-css' href="<?php echo BASEURL; ?>css/font-awesome.min.css" type='text/css' media='all'/>
        <link rel='stylesheet' id='font-awesome-css' href="<?php echo BASEURL; ?>css/web-style.css" type='text/css' media='all'/>

        <link rel='stylesheet' id='font-awesome-css' href="<?php echo BASEURL; ?>css/main.css" type='text/css' media='all'/>
        <link rel='stylesheet' id='font-awesome-css' href="<?php echo BASEURL; ?>css/style.css" type='text/css' media='all'/>
       
        <style id='hestia_style-inline-css' type='text/css'>a,
            .navbar.navbar-not-transparent .nav > li:not(.btn).on-section > a, 
            .navbar.navbar-not-transparent .nav > li.on-section:not(.btn) > a, 
            .navbar.navbar-not-transparent .nav > li.on-section:not(.btn):hover > a, 
            .navbar.navbar-not-transparent .nav > li.on-section:not(.btn):focus > a, 
            .navbar.navbar-not-transparent .nav > li.on-section:not(.btn):active > a, 
            body:not(.home) .navbar-default .navbar-nav > .active:not(.btn) > a,
            body:not(.home) .navbar-default .navbar-nav > .active:not(.btn) > a:hover,
            body:not(.home) .navbar-default .navbar-nav > .active:not(.btn) > a:focus,
            .hestia-blogs article:nth-child(6n+1) .category a, a:hover, .card-blog a.moretag:hover, .card-blog a.more-link:hover, .widget a:hover {color:#e91e63}.pagination span.current,.pagination span.current:focus,.pagination span.current:hover{border-color:#e91e63}button,
            button:hover,           
            input[type="button"],
            input[type="button"]:hover,
            input[type="submit"],
            input[type="submit"]:hover,
            input#searchsubmit, 
            .pagination span.current, 
            .pagination span.current:focus, 
            .pagination span.current:hover,
            .btn.btn-primary,
            .btn.btn-primary:link,
            .btn.btn-primary:hover, 
            .btn.btn-primary:focus, 
            .btn.btn-primary:active, 
            .btn.btn-primary.active, 
            .btn.btn-primary.active:focus, 
            .btn.btn-primary.active:hover,
            .btn.btn-primary:active:hover, 
            .btn.btn-primary:active:focus, 
            .btn.btn-primary:active:hover,
            .hestia-sidebar-open.btn.btn-rose,
            .hestia-sidebar-close.btn.btn-rose,
            .hestia-sidebar-open.btn.btn-rose:hover,
            .hestia-sidebar-close.btn.btn-rose:hover,
            .hestia-sidebar-open.btn.btn-rose:focus,
            .hestia-sidebar-close.btn.btn-rose:focus,
            .navbar .dropdown-menu li:hover > a,
            .label.label-primary,
            .hestia-work .portfolio-item:nth-child(6n+1) .label {background-color:#e91e63}@media (max-width:768px){.navbar .navbar-nav .dropdown a .caret{background-color:#e91e63}}button,.button,input[type="submit"],input[type="button"],.btn.btn-primary,.hestia-sidebar-open.btn.btn-rose,.hestia-sidebar-close.btn.btn-rose{-webkit-box-shadow: 0 2px 2px 0 rgba(233,30,99,0.14),0 3px 1px -2px rgba(233,30,99,0.2),0 1px 5px 0 rgba(233,30,99,0.12);box-shadow: 0 2px 2px 0 rgba(233,30,99,0.14),0 3px 1px -2px rgba(233,30,99,0.2),0 1px 5px 0 rgba(233,30,99,0.12)}.card .header-primary,.card .content-primary{background:#e91e63}.button:hover,button:hover,input[type="submit"]:hover,input[type="button"]:hover,input#searchsubmit:hover,.pagination span.current,.btn.btn-primary:hover,.btn.btn-primary:focus,.btn.btn-primary:active,.btn.btn-primary.active,.btn.btn-primary:active:focus,.btn.btn-primary:active:hover,.hestia-sidebar-open.btn.btn-rose:hover,.hestia-sidebar-close.btn.btn-rose:hover,.pagination span.current:hover{-webkit-box-shadow: 0 14px 26px -12pxrgba(233,30,99,0.42),0 4px 23px 0 rgba(0,0,0,0.12),0 8px 10px -5px rgba(233,30,99,0.2);box-shadow: 0 14px 26px -12px rgba(233,30,99,0.42),0 4px 23px 0 rgba(0,0,0,0.12),0 8px 10px -5px rgba(233,30,99,0.2);color:#fff}.form-group.is-focused .form-control{background-image: -webkit-gradient(linear,left top, left bottom,from(#e91e63),to(#e91e63)),-webkit-gradient(linear,left top, left bottom,from(#d2d2d2),to(#d2d2d2));background-image: -webkit-linear-gradient(#e91e63),to(#e91e63),-webkit-linear-gradient(#d2d2d2,#d2d2d2);background-image: linear-gradient(#e91e63),to(#e91e63),linear-gradient(#d2d2d2,#d2d2d2)}.navbar:not(.navbar-transparent) .navbar-nav > li:not(.btn) > a:hover, .navbar:not(.navbar-transparent) .navbar-nav > li.active:not(.btn) > a, .navbar:not(.navbar-transparent) .navbar-nav > li:not(.btn) > a:hover i {color:#e91e63}.hestia-top-bar,.hestia-top-bar .widget.widget_shopping_cart .cart_list{background-color:#363537}.hestia-top-bar .widget .label-floating input[type="search"]:-webkit-autofill{-webkit-box-shadow:inset 0 0 0px 9999px #363537}.hestia-top-bar,.hestia-top-bar .widget .label-floating input[type="search"],.hestia-top-bar .widget.widget_search form.form-group:before,.hestia-top-bar .widget.widget_product_search form.form-group:before,.hestia-top-bar .widget.widget_shopping_cart:before{color:#fff}.hestia-top-bar .widget .label-floating input[type="search"]{-webkit-text-fill-color:#fff!important}.hestia-top-bar a,.hestia-top-bar .top-bar-nav li a{color:#fff}.hestia-top-bar a:hover,.hestia-top-bar .top-bar-nav li a:hover{color:#eee}.woocommerce-cart table.shop_table th{font-size:10.8px}.added_to_cart.wc-forward,.products .shop-item .added_to_cart{font-size:11.6px}.woocommerce.single-product .product .woocommerce-product-rating .star-rating,.woocommerce .star-rating,.woocommerce .woocommerce-breadcrumb,button,input[type="submit"],input[type="button"],.btn,.woocommerce .single-product div.product form.cart .button,.woocommerce div#respond input#submit,.woocommerce a.button,.woocommerce button.button,.woocommerce input.button,.woocommerce div#respond input#submit.alt,.woocommerce a.button.alt,body.woocommerce button.button.alt,.woocommerce button.single_add_to_cart_button,#secondary div[id^="woocommerce_price_filter"] .button,.footer div[id^="woocommerce_price_filter"] .button,.tooltip-inner,.woocommerce.single-product article.section-text{font-size:13.8px}.woocommerce-cart table.shop_table th{font-size:14.8px}p,ul,li,select,table,.form-group.label-floating label.control-label,.form-group.label-placeholder label.control-label,.copyright,.woocommerce .product .card-product div.card-description p,#secondary div[id^="woocommerce_layered_nav"] ul li a,#secondary div[id^="woocommerce_product_categories"] ul li a,.footer div[id^="woocommerce_layered_nav"] ul li a,.footer div[id^="woocommerce_product_categories"] ul li a,#secondary div[id^="woocommerce_price_filter"] .price_label,.footer div[id^="woocommerce_price_filter"] .price_label,.woocommerce ul.product_list_widget li,.footer ul.product_list_widget li,ul.product_list_widget li,.woocommerce .woocommerce-result-count,.woocommerce div.product div.woocommerce-tabs ul.tabs.wc-tabs li a,.variations tr .label,.woocommerce.single-product .section-text,.woocommerce div.product form.cart .reset_variations,.woocommerce.single-product div.product .woocommerce-review-link,.woocommerce div.product form.cart a.reset_variations,.woocommerce-cart .shop_table .actions .coupon .input-text,.woocommerce-cart table.shop_table td.actions input[type="submit"],.woocommerce .cart-collaterals .cart_totals .checkout-button,.form-control,.woocommerce-checkout #payment ul.payment_methods li,.woocommerce-checkout #payment ul.payment_methods div,.woocommerce-checkout #payment ul.payment_methods div p,.woocommerce-checkout #payment input[type="submit"],.woocommerce-checkout input[type="submit"]{font-size:15.8px}#secondary div[id^="woocommerce_recent_reviews"] .reviewer,.footer div[id^="woocommerce_recent_reviews"] .reviewer{font-size:16.8px}.hestia-features .hestia-info p,.hestia-features .info p,.features .hestia-info p,.features .info p,.woocommerce-cart table.shop_table .product-name a,.woocommerce-checkout .form-row label,.media p{font-size:17.8px}.blog-post .section-text{font-size:18.6px}.hestia-about p{font-size:19.3px}.carousel span.sub-title,.media .media-heading,.card .footer .stats .fa{font-size:20px}table>thead>tr>th{font-size:22.8px}.woocommerce-cart table.shop_table th{font-size:10.8px}.added_to_cart.wc-forward,.products .shop-item .added_to_cart{font-size:11.6px}.woocommerce.single-product .product .woocommerce-product-rating .star-rating,.woocommerce .star-rating,.woocommerce .woocommerce-breadcrumb,button,input[type="submit"],input[type="button"],.btn,.woocommerce .single-product div.product form.cart .button,.woocommerce div#respond input#submit,.woocommerce a.button,.woocommerce button.button,.woocommerce input.button,.woocommerce div#respond input#submit.alt,.woocommerce a.button.alt,body.woocommerce button.button.alt,.woocommerce button.single_add_to_cart_button,#secondary div[id^="woocommerce_price_filter"] .button,.footer div[id^="woocommerce_price_filter"] .button,.tooltip-inner,.woocommerce.single-product article.section-text{font-size:13.8px}.woocommerce-cart table.shop_table th{font-size:14.8px}p,ul,li,select,table,.form-group.label-floating label.control-label,.form-group.label-placeholder label.control-label,.copyright,.woocommerce .product .card-product div.card-description p,#secondary div[id^="woocommerce_layered_nav"] ul li a,#secondary div[id^="woocommerce_product_categories"] ul li a,.footer div[id^="woocommerce_layered_nav"] ul li a,.footer div[id^="woocommerce_product_categories"] ul li a,#secondary div[id^="woocommerce_price_filter"] .price_label,.footer div[id^="woocommerce_price_filter"] .price_label,.woocommerce ul.product_list_widget li,.footer ul.product_list_widget li,ul.product_list_widget li,.woocommerce .woocommerce-result-count,.woocommerce div.product div.woocommerce-tabs ul.tabs.wc-tabs li a,.variations tr .label,.woocommerce.single-product .section-text,.woocommerce div.product form.cart .reset_variations,.woocommerce.single-product div.product .woocommerce-review-link,.woocommerce div.product form.cart a.reset_variations,.woocommerce-cart .shop_table .actions .coupon .input-text,.woocommerce-cart table.shop_table td.actions input[type="submit"],.woocommerce .cart-collaterals .cart_totals .checkout-button,.form-control,.woocommerce-checkout #payment ul.payment_methods li,.woocommerce-checkout #payment ul.payment_methods div,.woocommerce-checkout #payment ul.payment_methods div p,.woocommerce-checkout #payment input[type="submit"],.woocommerce-checkout input[type="submit"]{font-size:15.8px}#secondary div[id^="woocommerce_recent_reviews"] .reviewer,.footer div[id^="woocommerce_recent_reviews"] .reviewer{font-size:16.8px}.hestia-features .hestia-info p,.hestia-features .info p,.features .hestia-info p,.features .info p,.woocommerce-cart table.shop_table .product-name a,.woocommerce-checkout .form-row label,.media p{font-size:17.8px}.blog-post .section-text{font-size:18.6px}.hestia-about p{font-size:19.3px}.carousel span.sub-title,.media .media-heading,.card .footer .stats .fa{font-size:20px}table>thead>tr>th{font-size:22.8px}.widget h5{font-size:22.14px}.carousel h1.hestia-title,.carousel h2.title{font-size:67.5px}h1,.page-header.header-small .hestia-title,.page-header.header-small .title,.blog-post.blog-post-wrapper .section-text h1{font-size:53.5px}h2,.blog-post.blog-post-wrapper .section-text h2,.woocommerce section.related.products h2,.woocommerce.single-product h1.product_title{font-size:36.7px}h3,.blog-post.blog-post-wrapper .section-text h3{font-size:25.85px}h4,.card-blog .card-title,.blog-post.blog-post-wrapper .section-text h4{font-size:18.5px}h5,.blog-post.blog-post-wrapper .section-text h5{font-size:17.8px}h6,.blog-post.blog-post-wrapper .section-text h6,.card-product .category{font-size:12.9px}.archive .page-header.header-small .hestia-title,.blog .page-header.header-small .hestia-title,.search .page-header.header-small .hestia-title,.archive .page-header.header-small .title,.blog .page-header.header-small .title,.search .page-header.header-small .title{font-size:45.1px}.woocommerce span.comment-reply-title,.woocommerce.single-product .summary p.price,.woocommerce.single-product .summary .price,.woocommerce.single-product .woocommerce-variation-price span.price{font-size:25.85px}</style>
        <link rel='stylesheet' id='hestia_fonts-css' href='https://fonts.googleapis.com/css?family=Roboto%3A300%2C400%2C500%2C700%7CRoboto+Slab%3A400%2C700&#038;subset=latin%2Clatin-ext&#038;ver=1.1.45' type='text/css' media='all'/>
        <meta name="msapplication-TileImage" content="<?php echo BASEURL; ?>img/logo.png"/>
    </head>

    <body class="page-template-default page page-id-69 wp-custom-logo blog-post">
        <div class="wrapper">
            <header class="header" id="header">
                <nav class="navbar navbar-default navbar-fixed-top  hestia_left navbar-not-transparent">
                    <div class="container">
                        <div class="navbar-header">
                            <div class="title-logo-wrapper">
                                <a class="navbar-brand" href="<?php echo BASEURL; ?>" title="Hush"><img src="<?php echo BASEURL; ?>img/logo.png"></a>
                            </div>
                        </div>
                        <div id="main-navigation" class="collapse navbar-collapse">

                            <ul id="menu-primary-menu" class="nav navbar-nav navbar-right">
                                <li id="menu-item-102" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-102"><a style="font-family: AvenirMedium" title="Home" href="<?php echo BASEURL; ?>web/home">Home</a></li>
                                <li id="menu-item-63" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63"><a style="font-family: AvenirMedium" title="Privacy Policy" href="<?php echo BASEURL; ?>web/privicyPolicy">Privacy Policy</a></li>
                                <li id="menu-item-72" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-69 current_page_item menu-item-72"><a style="font-family: AvenirMedium" title="FAQs" href="<?php echo BASEURL; ?>web/faq">FAQs</a></li>

                                <li id="menu-item-102" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-102"><a style="font-family: AvenirMedium" title="Public Forum" href="<?php echo BASEURL; ?>GlobalContent">Global Wall</a></li>

                            </ul>
                        </div>						
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navigation">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="sr-only">Toggle Navigation</span>
                        </button>
                    </div>

                </nav>
            </header>



