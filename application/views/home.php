<div id="carousel-hestia-generic" class="carousel slide" data-ride="carousel">
    <div class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <div class="page-header " style="background-image:url(<?php echo BASEURL; ?>/img/useful.jpg)">
                    <div class="container" style="font-family: Helvetica">
                        <div class="row hestia-big-title-content">
                            <div class="col-md-6  
                                 text-left">
                                <h1  style="color: #DB4336; font-size: 100px; font-family: AvenirHeavy">Hush </h1>
                                <h2 class="hestia-title"  style=" font-size:32px;color: #DB4336; font-family: AvenirHeavy ;margin-left: 90px">Real Voice of Employees</h2>

                                <p style=" color: #DB4336; font-size:20px;margin-top:40px;font-family: AvenirBook">Discuss work issues openly !</p>

                                <div class="buttons1">
                                    </br>
                                    <a href="https://itunes.apple.com/us/app/hush-anonymous-office-talk/id1274005551?mt=8" title="Download Hush"  target="_blank">
                                        <img src="<?php echo BASEURL; ?>/img//apple-app-store-icon.png" style="height:60px;width:200px"/> 
                                    </a>
                                    <a href="https://play.google.com/store/apps/details?id=com.erbauen.hush" title="Download Hush"  target="_blank">
                                        <img src="<?php echo BASEURL; ?>/img/googleplay.png" style="height:64px;width:200px"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="main main-raised">
    <section class="features hestia-features" id="features" data-sorder="hestia_features">
        <div class="container">
            <div class="row">
<!--                <div class="col-md-8 col-md-offset-2">-->
                    <h2 class="title" style="font-family: AvenirHeavy">Enabling diversity of opinions in organizations</h2>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
<!--                </div>-->
            </div>
            <div class="row hestia-features-content">
                <div class="col-md-4"><img src="<?php echo BASEURL; ?>/img/intro/intro1.jpg"/></div>
                <div class="col-md-4"><img src="<?php echo BASEURL; ?>/img/intro/intro2.png"/></div>
                <div class="col-md-4"><img src="<?php echo BASEURL; ?>/img/intro/intro3.jpg"/></div>
            </div>
            <div class="row hestia-features-content">
                <div class="col-md-4 feature-box">
                    <div class="info hestia-info">
                        <div class="icon" style="color:#e91e63">
                            <img src="<?php echo BASEURL; ?>/img/icons/CompanyIcon.svg" width="46" height="46"/>
                        </div>
                        <h3 class="info-title" style="font-family: AvenirHeavy">Company Wall</h3>
                        <p style="font-family: AvenirBook">An exclusive forum for you and your verified coworkers to voice their real opinion. The forum is Anonymous to enable each opinion to be looked at in insolation without connecting it to the individual. Discuss Increments, Company Strategy, Layoffs, Promotions. Give "Bravos" to your peers. Upvote what questions your CEO should answer every week.</p>
                    </div>
                </div>
                <div class="col-md-4 feature-box">
                    <div class="info hestia-info">
                        <div class="icon" style="color:#00bcd4">
                            <img src="<?php echo BASEURL; ?>/img/icons/GlobalIcon.svg" width="64" height="64"/>
                        </div>
                        <h3 class="info-title" style="font-family: AvenirHeavy">Global Wall</h3>
                        <p style="font-family: AvenirBook">A common forum for verified employees of all companies. Find out about culture and salary at other places, Get career advice from others. Considering a job offer from a company? Anonymously ask the employees of that company any questions that come to your mind. Post job offers.</p>
                    </div>
                </div>
                <div class="col-md-4 feature-box">
                    <div class="info hestia-info">
                        <div class="icon">
                            <img src="<?php echo BASEURL; ?>/img/icons/You_icon.svg" width="48" height="48"/>
                        </div>
                        <h3 class="info-title" style="font-family: AvenirHeavy">Real People</h3>
                        <p style="font-family: AvenirBook">Verified employees from companies. You are allocated a random anonymous handle when you install the app. No one can find out your real identity. Not even the Hush team! Hush is the Real voice of employees. Strong community based moderation to remove posts the community is not ok with.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="about hestia-about 
             " id="about" data-sorder="hestia_about">
        <div class="container">
            <div class="row hestia-about-content">
            </div>
        </div>
    </section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>

        $(window).on("scroll", function () {

            if ($(this).scrollTop() > 100) {
                $("container").css("background", "#252525");
            } else {
                $("container").css("background", "#fff");
            }
        });
    </script>