<div id="primary" class="boxed-layout-header page-header  header-small" data-parallax="active" style="background-image:url(<?php echo BASEURL; ?>/img/faq.jpg)">


    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="hestia-title" style=" font-family: AvenirHeavy">FAQs</h1>
            </div>
        </div>
    </div>
</div>
<div class="main main-raised">
    <div class="blog-post ">
        <div class="container">


            <article id="post-69" class="section section-text">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 page-content-wrap">
                        <h3 style="font-family: AvenirMedium"><span style="color: #ff686a;">What is the Hush App all about?</span></h3>
                        <div>
                            <p style="font-family: AvenirBook">Hush is an app for your workplace. We want to facilitate open conversations between professionals in the same company or with others in the  industry. The idea is to create a place where relevant topics can be discussed without any fear of retribution.</p>
                            <p style="font-family: AvenirBook">It is also a tool for the executive team to get a pulse of their employees on a daily basis and not wait for a quarterly / half yearly survey. Unlike the typical employee surveys here the questions / discussions are initiated by employees so be prepared to see some uncomfortable questions and responses.</p>
                            <h3 style="font-family: AvenirMedium"><span style="color: #ff686a;">Is Hush really anonymous?</span></h3>
                            <p style="font-family: AvenirBook">Yes we want to facilitate open discussions and that&#8217;s why we don&#8217;t ever send you any emails to your work email or capture your real name anywhere. It is our biggest priority to ensure that you can always use the app anonymously.</p>
                            <p style="font-family: AvenirBook"><strong>Company Email based verification : </strong> We authenticate you by sending a one time code to your office email. We don&#8217;t store your email id so there is no way to tie your activity on the app to your email.</p>
                            <p style="font-family: AvenirBook"><strong>LinkedIn based verification : </strong>  We let you authenticate via LinkedIn by making your company email as the primary email on LinkedIn. We don&#8217;t send any verification email to you if you use this option as the email is sent by LinkedIn when you change your primary account there. We use your primary email address on LinkedIn to verify on the fly and don&#8217;t store it  so there is no way to tie your activity on the app to your email.</p>
                            <h3 style="font-family: AvenirMedium"><span style="color: #ff686a;">Why do I have to verify with LinkedIn / Email?</span></h3>
                            <p style="font-family: AvenirBook">You need to verify with LinkedIn / Email because we want to ensure only authentic company employees get access to their company wall</p>
                            <h3 style="font-family: AvenirMedium"><span style="color: #ff686a;">How do you take care of bullying / racism etc. ?</span></h3>
                            <p style="font-family: AvenirBook">This is something we take very seriously. Any content which is flagged multiple times is removed. People who keep posting content that is objectionable are removed from the platform. Hush is a completely anonymous forum but now we are also moving towards using a mix of community based moderation and Machine Learning to ensure this cannot become a forum for bullying / harassment .
                                <br>If you want to report any content on the app you can also send an email to <a href="mailto:contact@voyageup.com"  target="_blank"><b style="color: #ff686a;">contact@voyageup.com</b></a> </p>
                            <h3 style="font-family: AvenirMedium"><span style="color: #ff686a;">Can I delete my posts or comments? </span></h3>
                            <p style="font-family: AvenirBook">Yes. Once you delete something it is lost forever and cannot be recovered</p>
                            <h3 style="font-family: AvenirMedium"><span style="color: #ff686a;">Is there any feature for HR / Leadership team? </span></h3>
                            <p style="font-family: AvenirBook">Yes. We can provide your overall sentiment analysis and trends around what your employees are thinking. We can’t provide any data that could be used to find out any user’s identity.</p>
                            <p style="font-family: AvenirBook">&nbsp;</p>
                        </div>
                    </div>
                </div>
            </article>
            <div class="section section-blog-info">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="row">
                            <div class="col-md-12">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

