
<div id="loader-wrapper">
    <div id="loader"></div>

    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div>
<body>
    <div class="feed-container">
        <div  id="content">

        </div>

        <div  id="output_comments">

        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">For true anonymity,
                        Hush is for mobile only.</h3>
                </div>
                <div class="modal-body">
                   
                    <h4>Please download and join the community.</h4>
                    <div class="modal-footer">

                        <div class="row store">
                            <div class="col-sm-6 ">

                                <a href="http://bit.ly/2x4YLJ4" target="_blank">

                                    <img src="https://www.vr.fi/cs/vr/img/d/1/1/googleplay_EN.png?blobnocache\u003dfalse" >
                                </a>
                            </div>
                            <div class="col-sm-5 ">
                                <a href="http://bit.ly/2x4YLJ4" target="_blank">
                                    <img src="http://yorkcars-taxis.co.uk/wp-content/uploads/2017/01/apple-app-store-icon-e1485370451143.png" >
                                </a>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script  src="<?php echo BASEURL; ?>js/script.js"></script>

<script>

    $(document).ready(function () {
        $.ajaxSetup({cache: true});
        $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
            FB.init({
                appId: '<?php echo FACEBOOK_KEY ?>',
                version: 'v2.7' // or v2.1, v2.2, v2.3, ...
            });

        });
    });



    window.onload = function () {
        loadData();
    };

    function loadData() {
        var base_url = '<?php echo BASEURL ?>';
        var type = '<?php echo $type ?>';
        var id = '<?php echo $id ?>';
        $(document).ready(function () {

            jQuery.support.cors = true;
            $.ajax(
                    {
                        type: "GET",
                        url: base_url + "index.php/GlobalContent/getComments",
                        data: {type: type,
                            id: id},
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: true,
                        success: function (data) {

                            //alert('success');
                            var output = document.getElementById('content');

                            //alert("data "+data['aData'][0]);
                            //for (var i in data['pData'])
                            {
                                var i = 0;
                                // alert(data['aData'][i].subject);
                                var ele = document.createElement("div");
                                ele.setAttribute("class", "post4-info");

                                if (data['pData'][i].type === 'discussion') {
                                    var post = "<h3>" + data['pData'][i].subject + "</h3>";
                                    post += "<p>" + data['pData'][i].post + "</p>";

                                    if (typeof data['pData'][i].imageUrl !== "undefined" &&
                                            data['pData'][i].imageUrl !== "") {
                                        post += "<img src='" + data['pData'][i].imageUrl + "'></img>";
                                    }

                                    ele.innerHTML = post;
                                } else {

                                    ele.innerHTML = "<h3> " + data['pData'][i].poll + "</h3>";
                                    var pollView = document.createElement("div");
                                    pollView.setAttribute("class", "pollView");
                                    pollView.innerHTML = "<h3 style='font-size:1.1em; color:#FF474A;'>Votes : " + data['pData'][i].votingCount + "</h3><h3 style='font-size:1.1em;'>You can select only one answer</h3>";

                                    ele.appendChild(pollView);

                                    var pollOptions = document.createElement("div");
                                    pollOptions.setAttribute("class", "pollOptionView");
                                    var options = "";
                                    for (var j in data['pData'][i].options)
                                    {

                                        options += "<h3 style='font-size:1.2em;' data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon glyphicon-unchecked' aria-hidden='true'></span>  " + data['pData'][i].options[j].option + "</h3><br>";


                                    }

                                    pollOptions.innerHTML = "<form>" + options + "</form>"
                                    ele.appendChild(pollOptions);

                                }

                                var handle = document.createElement("div");
                                handle.setAttribute("class", "handle");
                                handle.innerHTML = "<h3>" + data['pData'][i].userName + "- " + data['pData'][i].companyName + "</h3>"
                                ele.appendChild(handle);

                                var commentCount = "0";
                                if (typeof data['pData'][i].commentCount !== "undefined" &&
                                        data['pData'][i].commentCount !== "") {

                                    commentCount = data['pData'][i].commentCount;

                                }

                                var options = document.createElement("div");
                                options.setAttribute("class", "comments cmnt");
                                options.innerHTML = "<h3><ul><li><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span> " + data['pData'][i].viewCount + "</li>" +
                                        "<li data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-thumbs-up' aria-hidden='true'></span> " + data['pData'][i].likeCount + " </li>" +
                                        "<li data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-comment' aria-hidden='true'></span> " + commentCount + " </li>" +
                                        "<li><span class='glyphicon'><img src='http://simpleicon.com/wp-content/uploads/facebook-2.png' onclick=onClickFacebook(" + data['pData'][i].pollId + "," + data['pData'][i].discussionId + ");></img></span></li>" +
                                        "<li><span class='glyphicon'><img src='http://www.pngmart.com/files/5/Twitter-PNG-Transparent.png' onclick=tweetCurrentPage();></img></span></li></ul></h3>"

                                ele.appendChild(options);
                                output.appendChild(ele);


                            }

                            var output_comments = document.getElementById('output_comments');

                            //alert(data['aData'].length);
                            if (data.code === 555) {

                                var comments_title = document.createElement("div");
                                comments_title.setAttribute("class", "cmt-heading");
                                comments_title.innerHTML = "<h3> Comments:</h3>";

                                output.appendChild(comments_title);

                            }
                            for (var i in data['aData']) {

                                var comments = document.createElement("div");
                                comments.setAttribute("class", "post4-comments");

                                comments.innerHTML = "<h3 style='font-size:1.3em; padding-bottom:10px;'>" + data['aData'][i].userName +"-"+data['aData'][i].companyName+ "</h3><p>" + data['aData'][i].comment + "</p>";
                                output_comments.appendChild(comments);

                            }

                            $('body').addClass('loaded');
                            $('h1').css('color', '#222222');


                        },
                        error: function (msg) {
                            alert('succefailuress');
                        }
                    });
        });
    }


    function onClickFacebook(poll_id, discussion_id, type) {
        var base_url = '<?php echo BASEURL ?>';
        var type = "poll";
        var id = poll_id;
        if (typeof poll_id === "undefined") {
            type = "post";
            id = discussion_id;
        }

        shareOnFacebook(base_url, type, id);
    }



</script>



