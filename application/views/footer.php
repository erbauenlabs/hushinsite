<footer class="footer footer-black footer-big">
            
                <div class="buttons1">
                    <a href="https://www.facebook.com/thehushapp" title="facebook"  target="_blank">
                        <img src="<?php echo BASEURL; ?>/img/icons/facebook.png" style="height:60px;width:60px"/> 
                    </a>
                    <a href="https://www.instagram.com/hush_make_work_better/" title="instagram"  target="_blank">
                        <img src="<?php echo BASEURL; ?>/img/icons/instagram.png" style="height:60px;width:60px"/>
                    </a>
                    
                     <a href="https://twitter.com/TheHushApp" title="twitter"  target="_blank">
                        <img src="<?php echo BASEURL; ?>/img/icons/twitter.png" style="height:60px;width:60px"/>
                    </a>
                </div>

</footer>
<div style="display: none">
</div>
</div>
</div>

</body>

</html>