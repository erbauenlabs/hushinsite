<div id="primary" class="boxed-layout-header page-header  header-small" data-parallax="active" style="background-image:url(<?php echo BASEURL; ?>/img/PrivacyPolicy.jpeg)">


    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="hestia-title" style=" font-family: AvenirHeavy">Privacy Policy</h1>			
            </div>
        </div>
    </div>
</div>

<div class="main main-raised">
    <div class="blog-post ">
        <div class="container">
            <article id="post-59" class="section section-text">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 page-content-wrap">
                        <header class="entry-header">
                            <h2 class="entry-title single-title" style="font-family: AvenirMedium"><span style="color: #ff686a;">Usage guidelines and Privacy Policy</span></h2></br>
                            <div class="clearfix" style="margin-bottom: 10px"><span style="color: #ff686a;font-family: AvenirMedium ; font-size: 25px">Usage Guidelines</span></div>
                        </header>
                        <div class="entry-content content-page ">
                            <p style="font-family: AvenirBook ;margin-top: 10px">Hush is an application owned by Erbauen Labs. It is intended to be used by people who are 21 years or older and are working for a company / startup.</p>
                            <p style="font-family: AvenirBook; margin: 0px">What kind of content should not be posted:</p>
                            <ul style="font-family: AvenirBook ;margin-left: 25px">
                                <li>Pornographic</li>
                                <li>Threats</li>
                                <li>Harassment or Bullying</li>
                                <li>Racist</li>
                                <li>Content not related to company or career</li>
                            </ul>
                            <p style="font-family: AvenirBook; margin: 5px">Violating these rules will result in the removal of content. It can also lead to the suspension of your account and being prohibited from using Hush in the future altogether.</p>

                            <p class="pri-sub" style="font-family: AvenirMedium ;color: #ff686a;margin:10px 0px 0px 0px"><span>Usage Data :</span></p>
                            <p style="font-family: AvenirBook; ">By using the Hush application you agree to let Hush collect information about your usage of the application. It also includes which Wifi network you are connected to</p>

                            <p class="pri-sub" style="font-family: AvenirMedium ;color: #ff686a;margin:10px 0px 0px 0px"><span>Severability :</span></p>
                            <p style="font-family: AvenirBook; ">If any provision of these Terms shall be deemed unlawful, void or for any reason unenforceable, then that provision shall be deemed severable from these Terms and shall not affect the validity and enforceability of any remaining provisions.</p>

                            <p class="pri-sub" style="font-family: AvenirMedium ;color: #ff686a;margin:5px 0px 0px 0px"><span>Complete Agreement :</span></p>
                            <p style="font-family: AvenirBook; ">These Terms supersede all prior understandings regarding the provision of the Services and save for any fraudulent misrepresentation represent the complete agreement between you and us.</p>

                            <h3 style="color: #ff686a;font-family: AvenirHeavy ;margin:5px 0px 0px 0px"><span >Privacy Policy</span></h3>
                            <h5 style="color: #ff686a;font-family: AvenirMedium ;margin:15px 0px 0px 0px"><span >Information We Collect- what you directly provide us</span></h5>
                            <p style="font-family: AvenirBook ;margin:15px 0px 0px 0px">For you to become a Hush user we need to collect a few important details about you :</p>
                            <ul style="font-family: AvenirBook ;margin-left: 25px">
                                <li>Mobile Device Id</li>
                                <li>Wi-Fi network you are connected to</li>
                                <li>We temporarily use your email to verify your employment if you join your company wall via LinkedIn or the company email route. The email address is just used for authenticating your employment and we don&#8217;t store it on our servers.</li>
                            </ul>
                            <p class="pri-sub" style="font-family: AvenirMedium ;color: #ff686a;margin:30px 0px 0px 0px">What You Automatically Provide Us When You Use Our Services</p>
                            <p style="font-family: AvenirBook;margin:5px 0px 10px 0px">There is some information we collect automatically when you access our Services :</p>
                            
                            <ul style="font-family: AvenirBook ; margin-left: 25px">
                                <li><strong>Usage Information.</strong> We collect information about your activity and the content you send and receive through our Services. For example, we collect information such as the time, date, device which posted the content.</li>
                                <li><strong>Device Information.</strong> We collect information about your device, including the hardware model, operating system and version and browser language. We also collect certain device information that will help us diagnose problems in the hopefully rare event you experience any crash or other problem while using our Services.</li>
                                <li><strong>Wifi network Information.</strong> We collect information about your Wifi network because that is the source through which we allow you to join a particular company&#8217;s community. We never share your Wifi usage data with third parties</li>
                            </ul>
                           
                            <p class="pri-sub" style="font-family: AvenirMedium ;color: #ff686a;margin:15px 0px 0px 0px"><span style="color: #ff686a;">How We Use Information</span></p>
                            <ul style="font-family: AvenirBook ;margin-left: 25px">
                                <li>Monitor and analyse trends and usage;</li>
                                <li>Develop, improve, and refine products, services, and functionality</li>
                            </ul>
                            <p class="pri-sub" style="font-family: AvenirMedium ;color: #ff686a;margin:15px 0px 0px 0px"><span style="color: #ff686a;">How We Share Information</span></p>
                            <p style="font-family: AvenirBook;margin:5px 0px 10px 0px">Here is the type of information we may share about you with others:</p>
                            <ul style="font-family: AvenirBook ;margin-left: 25px">
                                <li>When you post a message the other users will see whatever your posted message says</li>
                                <li>We also display your comments on a post by another member</li>
                            </ul>
                            <p class="pri-sub" style="font-family: AvenirMedium ;color: #ff686a;margin:15px 0px 0px 0px"><span style="color: #ff686a;">Analytics and Advertising Services Provided by Others</span></p>
                            <p style="font-family: AvenirBook;margin:5px 0px 10px 0px">We may let other companies use cookies, web beacons, and other technologies to advertise on our behalf. These companies may collect information about how you use other websites and online services over time and across different services. The information collected may include unique device identifiers, device manufacturer and operating system, IP address, browser type, pages viewed, session start/stop time, links clicked, and conversion information. This information may be used to, among other things, analyze and track data, determine the popularity of certain content, and better understand your online activity.</p>
                        </div>
                    </div>
                </div>
            </article>
            <div class="section section-blog-info">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="row">
                            <div class="col-md-12">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
