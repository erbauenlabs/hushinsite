
<div id="loader-wrapper">
    <div id="loader"></div>

    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div>
<body>


    <div class="feed-container">
        <div  id="content">

        </div>


        <div class="pagination pagnation-center" id="pagnation">


        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">For true anonymity,
                        Hush is for mobile only.</h3>
                </div>
                <div class="modal-body">

                    <h4>Please download and join the community.</h4>
                    <div class="modal-footer">

                        <div class="row store">
                            <div class="col-sm-6 ">

                                <a href="http://bit.ly/2x4YLJ4" target="_blank">

                                    <img src="https://www.vr.fi/cs/vr/img/d/1/1/googleplay_EN.png?blobnocache\u003dfalse" >
                                </a>
                            </div>
                            <div class="col-sm-5 ">
                                <a href="http://bit.ly/2x4YLJ4" target="_blank">
                                    <img src="http://yorkcars-taxis.co.uk/wp-content/uploads/2017/01/apple-app-store-icon-e1485370451143.png" >
                                </a>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>


    window.onload = function () {
        loadData();
    };

    function loadData() {
        var base_url = '<?php echo BASEURL ?>';
        var count = 230;

        var pages = window.location.href.split('=');
        var current_page = "1";
        if (pages.length > 1)
            current_page = pages[1].trim();

        $(document).ready(function () {

            jQuery.support.cors = true;
            $.ajax(
                    {
                        type: "GET",
                        url: base_url + "index.php/GlobalContent/getWallData",
                        data: {page: current_page},
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: true,
                        success: function (data) {

                            var output = document.getElementById('content');

                            //alert("data "+data['aData'][0]);
                            for (var i in data['aData']) {
                                // alert(data['aData'][i].subject);
                                var ele = document.createElement("div");
                                ele.setAttribute("class", "post4-info");

                                if (data['aData'][i].type === 'discussion') {
                                    var post = "<h3><a href=GlobalContent/comment/post/" + data['aData'][i].discussionId + "/" + data['aData'][i].subject.replace(/ /g, "-") + ">" + data['aData'][i].subject + "</a></h3>";
                                    post += "<p>" + data['aData'][i].post + "</p>";

                                    if (typeof data['aData'][i].imageUrl !== "undefined" &&
                                            data['aData'][i].imageUrl !== "") {
                                        post += "<img src='" + data['aData'][i].imageUrl + "'></img>";
                                    }

                                    ele.innerHTML = post;
                                } else {

                                    ele.innerHTML = "<h3><a href=GlobalContent/comment/poll/" + data['aData'][i].pollId + "/" + data['aData'][i].poll.replace(/ /g, "-") + ">" + data['aData'][i].poll + "</a></h3>";
                                    var pollView = document.createElement("div");
                                    pollView.setAttribute("class", "pollView");
                                    pollView.innerHTML = "<h3 style='font-size:1.1em; color:#FF474A;'>Votes : " + data['aData'][i].votingCount + "</h3><h3 style='font-size:1.1em;'>You can select only one answer</h3>";

                                    ele.appendChild(pollView);

                                }




                                var handle = document.createElement("div");
                                handle.setAttribute("class", "handle");
                                handle.innerHTML = "<h3>" + data['aData'][i].userName + " - " + data['aData'][i].companyName + "</h3>"
                                ele.appendChild(handle);
                                //output.appendChild(ele);
                                //string newDate = moment(data['aData'][i].createdDate , "YYYY-MM-DD hh:mm:ss").format("MM/DD/YY")

                                var commentCount = "0";
                                if (typeof data['aData'][i].commentCount !== "undefined" &&
                                        data['aData'][i].commentCount !== "") {

                                    commentCount = data['aData'][i].commentCount;

                                }


                                var options = document.createElement("div");
                                options.setAttribute("class", "comments cmnt");
                                options.innerHTML = "<h3><ul><li><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span> " + data['aData'][i].viewCount + "</li>" +
                                        "<li data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-thumbs-up' aria-hidden='true'></span> " + data['aData'][i].likeCount + " </li>" +
                                        "<li data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-comment' aria-hidden='true'></span> " + commentCount + " </li>" +
                                        "</ul></h3>"
                                ele.appendChild(options);

                                output.appendChild(ele);

                            }

                            var prevPage = (parseInt(current_page) - 1)
                            if (prevPage < 1)
                                prevPage = 1;
                            var pagerHtml = "<a  href = GlobalContent?page=" + prevPage + " > &laquo; </a>"
                            var j = 0;

                            //int max_page= count/20;

                            //alert(max_page);

                            if (current_page > 5)
                                j = (current_page - 5) * 20;


                            var pageCount = 0;
                            while (j < count) {

                                pageCount += 1;
                                j += 20;
                                var page = j / 20;

                                if (page.toString() === current_page)
                                    pagerHtml += "<a class = 'active' href = GlobalContent?page=" + page + " > " + page + " </a>";
                                else
                                    pagerHtml += "<a href = GlobalContent?page=" + page + " > " + page + " </a>";

                                if (pageCount === 10) {
                                    break;
                                }

                            }


                            var nextPage = parseInt(current_page) + 1;
                            if (nextPage > (Math.ceil(count / 20)))
                                nextPage = Math.ceil(count / 20);

                            pagerHtml += " <a href = GlobalContent?page=" + nextPage + "  > &raquo; </a>";


                            var pager = document.getElementById('pagnation');

                            var pageCount = document.createElement("div");

                            pageCount.innerHTML = pagerHtml;
                            pager.appendChild(pageCount);

                            $('body').addClass('loaded');
                            $('h1').css('color', '#222222');


                        },
                        error: function (msg) {

                        }
                    });
        })
    }


</script>



