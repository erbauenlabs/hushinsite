/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function shareOnFacebook(base_url, type, id) {

    $.ajax({
        type: "GET",
        url: base_url + "index.php/GlobalContent/getDeepLinkUrl",
        data: {url: window.location.href,
            type: type, id: id, deeplink: true},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: true,
        success: function (data) {

            FB.ui({
                method: 'share',
                display: 'popup',
                href: data.url,
            }, function (response) {

                if (response && !response.error_message) {
                    alert('Thanks for the support!!!');
                }
            });


        },
        error: function (msg) {
            alert('Error');
        }
    });

}


function tweetCurrentPage(base_url, type, id)
{

    window.open("https://twitter.com/share?url=" + escape(window.location.href), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');


}


